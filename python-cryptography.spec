%global srcname cryptography
Name:           python-%{srcname}
Version:        3.3.1
Release:        2
Summary:        PyCA's cryptography library
License:        ASL 2.0 or BSD
URL:            https://cryptography.io/en/latest/  
Source0:        %{pypi_source}

Patch6000:      backport-CVE-2020-36242.patch

BuildRequires:  openssl-devel
BuildRequires:  gcc

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-pytest >= 3.2.1
BuildRequires:  python%{python3_pkgversion}-setuptools
BuildRequires:  python%{python3_pkgversion}-pretend
BuildRequires:  python%{python3_pkgversion}-iso8601
BuildRequires:  python%{python3_pkgversion}-cryptography-vectors = %{version}
BuildRequires:  python%{python3_pkgversion}-asn1crypto >= 0.21
BuildRequires:  python%{python3_pkgversion}-hypothesis >= 1.11.4
BuildRequires:  python%{python3_pkgversion}-pytz
BuildRequires:  python%{python3_pkgversion}-idna >= 2.1
BuildRequires:  python%{python3_pkgversion}-six >= 1.4.1
BuildRequires:  python%{python3_pkgversion}-cffi >= 1.7

%description
cryptography is a package designed to expose cryptographic primitives and
recipes to Python developers.


%package     -n python%{python3_pkgversion}-cryptography
Summary:        PyCA's cryptography library

Requires:       openssl-libs
Requires:       python%{python3_pkgversion}-idna >= 2.1
Requires:       python%{python3_pkgversion}-asn1crypto >= 0.21
Requires:       python%{python3_pkgversion}-six >= 1.4.1
Requires:       python%{python3_pkgversion}-cffi >= 1.7

%{?python_provide:%python_provide python%{python3_pkgversion}-cryptography}

%description -n python%{python3_pkgversion}-cryptography
cryptography is a package designed to expose cryptographic primitives and
recipes to Python developers.

%package_help

%prep
%autosetup -n cryptography-%{version} -p1

%build
%py3_build

%install
%py3_install

%check
PYTHONPATH=%{buildroot}%{python3_sitearch} %{__python3} -m pytest -k "not (test_buffer_protocol_alternate_modes or test_dh_parameters_supported or test_load_ecdsa_no_named_curve)"

%files -n python%{python3_pkgversion}-cryptography
%defattr(-,root,root)
%doc AUTHORS.rst
%license LICENSE LICENSE.APACHE LICENSE.BSD
%{python3_sitearch}/*
%{python3_sitearch}/cryptography-%{version}-py*.egg-info

%files help
%defattr(-,root,root)
%doc README.rst docs

%changelog
* Tue Feb 23 2021 shixuantong <shixuantong@huawei.com> - 3.3.1-2
- fix CVE-2020-36242

* Mon Feb 1 2021 liudabo <liudabo1@huawei.com> - 3.3.1-1
- upgrade version to 3.3.1

* Tue Aug 11 2020 tianwei<tianwei12@huawei.com> -3.0-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete python2  

* Thu Jul 23 2020 dingyue<dingyue5@huawei.com> -3.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:NA

* Thu Apr 16 2020 chengquan<chengquan3@huawei.com> -2.9-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade software to v2.9

* Thu Feb 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.6.1-1
- Update to 2.6.1

* Tue Oct 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.3-5
- Package rebuild.

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.3-4
- Type: enhancement
- ID:   NA
- SUG:  NA
- DESC: fix build failed.

* Sat Sep 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.3-3
- Package init.
